package Vicings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapIsland {

    public Map<String, List<String>> islandgen(){

        Map<String, List<String>> mapgen = new HashMap<>();
        mapgen.put("Остров1", List.of("Остров3", "Остров4", "Остров2", "Остров6"));
        mapgen.put("Остров2", List.of("Остров1"));
        mapgen.put("Остров3", List.of("Остров1"));
        mapgen.put("Остров4", List.of("Остров1", "Остров5"));
        mapgen.put("Остров5", List.of("Остров6", "Остров4"));
        mapgen.put("Остров6", List.of("Остров1", "Остров5"));
        return mapgen;
    }
}
